﻿using ApplicationLayer.DTOs;
using DomainLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationLayer.Contracts
{
    public interface IBeneficiaryTemp
    {
        //Task<ServiceResponse> AddAsync(Employee employee);
        //Task<int> AddPerson(Employee person);

        Task AddBeneficiary(List<BeneficiaryTemp> beneficiaries);
        Task<List<BeneficiaryTemp>> GetAsync();
        Task<BeneficiaryTemp> GetByIdAsync(int id);
    }
}
