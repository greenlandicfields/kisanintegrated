﻿using Microsoft.Extensions.Localization;
using System.Reflection;

namespace BirsaKisanApp.PresentaionLayer.WebUI.Services
{
    public class SharedResource
    {
    }

    public class LanguageService
    {
        private readonly IStringLocalizer _localizer;
        private readonly IStringLocalizer _NameTaglocalizer;
        private readonly IStringLocalizer _StatusPagelocalizer;

        public LanguageService(IStringLocalizerFactory factory)
        {
            var type = typeof(SharedResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = factory.Create("SharedResource", assemblyName.Name);
            _NameTaglocalizer = factory.Create("NameTag", assemblyName.Name);
            _StatusPagelocalizer = factory.Create("StatusLabel", assemblyName.Name);
        }

        public LocalizedString Getkey(string key)
        {
            return _localizer[key];
        }
        public LocalizedString GetNameTag(string key)
        {
            return _NameTaglocalizer[key];
        }
        public LocalizedString GetStatus(string key)
        {
            return _StatusPagelocalizer[key];
        }
    }
}
