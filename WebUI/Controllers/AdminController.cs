﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using BirsaKisanApp.DomainLayer.Entities;
using Dapper;

namespace BirsaKisanApp.PresentaionLayer.WebUI.Controllers
{

    public class AdminController : Controller
    {
        private readonly IConfiguration _configuration;
        string? ConnectionString;

        public AdminController(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DapperConnection");
        }

        [Authorize]
        public IActionResult Index()
        {

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                try
                {
                    string query = "SELECT Id, ProjectName, UrlName, PersonInCharge, ContactNumber, Email from [dbo].[APIManagement]";

                    // Execute the query and retrieve the results
                    List<APIDetails> apiData = connection.Query<APIDetails>(query).ToList();
                    connection.Close();
                    return View(apiData);

                    

                }
                catch (Exception ex)
                {
                    return Ok(new { Error = ex.Message });
                }
            }

        }
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public IActionResult Create(APIDetails form)
        {


            if (ModelState.IsValid)
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        string query = "INSERT INTO [dbo].[APIManagement] (ProjectName, UrlName, PersonInCharge, ContactNumber, Email, CreatedDate,UpdatedTime) VALUES (@ProjectName, @UrlName, @PersonInCharge, @ContactNumber, @Email, GetDate(),GetDate());";

                        // Execute the query and retrieve the results
                        APIDetails apiDetail = connection.Query<APIDetails>(query, form).FirstOrDefault();
                        connection.Close();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        return Ok(new { Error = ex.Message });
                    }

                }
            }
            return View(form);
        }
        [Authorize]
        public IActionResult Edit(int Id)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                try
                {
                    string query = "SELECT Id, ProjectName, UrlName, PersonInCharge, ContactNumber, Email from [dbo].[APIManagement] where Id =@Id";

                    // Execute the query and retrieve the results
                    APIDetails apiDetail = connection.Query<APIDetails>(query, new { Id }).FirstOrDefault();
                    connection.Close();
                    return View(apiDetail);

                }
                catch (Exception ex)
                {
                    return Ok(new { Error = ex.Message });
                }
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult Edit(APIDetails form)
        {
            // Handle form submission (e.g., save to database)
            if (ModelState.IsValid)
            {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    try
                    {
                        string query = "UPDATE APIManagement SET ProjectName = @ProjectName, UrlName = @UrlName, PersonInCharge = @PersonInCharge, ContactNumber = @ContactNumber, Email =@Email, UpdatedTime = GETDATE() WHERE Id = @Id;";

                        // Execute the query and retrieve the results
                        APIDetails apiDetail = connection.Query<APIDetails>(query, form).FirstOrDefault();
                        connection.Close();
                        return RedirectToAction("Index");

                    }
                    catch (Exception ex)
                    {
                        return Ok(new { Error = ex.Message });
                    }
                    
                }
            }
            return View(form);
        }
            [Authorize]
            public IActionResult Delete(int Id)
            {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                try
                {
                    string query = "DELETE FROM [dbo].[APIManagement] where Id =@Id";

                    // Execute the query and retrieve the results
                    APIDetails apiDetail = connection.Query<APIDetails>(query, new { Id }).FirstOrDefault();
                    connection.Close();
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    return Ok(new { Error = ex.Message });
                }
            }
        }
        
    }
}
