﻿using BirsaKisanApp.DomainLayer.Entities;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;

namespace BirsaKisanApp.PresentaionLayer.WebUI.Controllers
{
    public class FarmerRegistrationController : Controller
    {
        string? ConnectionString;
        private readonly IConfiguration _configuration;
        public FarmerRegistrationController(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = _configuration.GetConnectionString("DapperConnection");
        }
        public IActionResult Index(Lgdcode code)
        {
            List<Lgdcode> dummyData = new List<Lgdcode>
            {
                new Lgdcode { DistrictId = 1, DistrictName = "District 1", BlockId = 101, BlockName = "Block 1", VillageId = 1001, VillageName = "Village 1" },
                new Lgdcode { DistrictId = 1, DistrictName = "District 1", BlockId = 102, BlockName = "Block 2", VillageId = 1002, VillageName = "Village 2" },
                new Lgdcode { DistrictId = 2, DistrictName = "District 2", BlockId = 201, BlockName = "Block 1", VillageId = 2001, VillageName = "Village 1" },
                new Lgdcode { DistrictId = 2, DistrictName = "District 2", BlockId = 202, BlockName = "Block 2", VillageId = 2002, VillageName = "Village 2" },
                // Add more dummy data as needed
            };
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                try
                {
                    string query = "SELECT Id, ProjectName, UrlName, PersonInCharge, ContactNumber, Email from [dbo].[APIManagement]";

                    // Execute the query and retrieve the results
                    List<Lgdcode> apiData = connection.Query<Lgdcode>(query).ToList();
                    connection.Close();
                    return View(apiData);

                }
                catch (Exception ex)
                {
                    return Ok(new { Error = ex.Message });
                }
            }
            return View(dummyData);
        }
    }
}
