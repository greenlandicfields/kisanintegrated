﻿using Microsoft.AspNetCore.Mvc;

namespace BirsaKisanApp.PresentaionLayer.WebUI.Controllers
{
    public class DirectorateController : Controller
    {
        public IActionResult Agriculture()
        {
            return View();
        }
        public IActionResult Horticulture()
        {
            return View();
        }
        public IActionResult SoilConservation()
        {
            return View();
        }
        public IActionResult Cooperative()
        {
            return View();
        }
        public IActionResult AnimalHusbandry()
        {
            return View();
        }
        public IActionResult Fisheries()
        {
            return View();
        }
        public IActionResult Dairy()
        {
            return View();
        }
        public IActionResult JSAMB()
        {
            return View();
        }
    }
}
