﻿//Time On Top Bar
(function ($) {

    $(document).ready(function ($) {
        datedisplay();
        renderTime();
        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
        });
    });

    function datedisplay() {
        var d = new Date()
        var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
        var monthname = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        $('#datedisplay').text(weekday[d.getDay()] + ", " + d.getDate() + " " + monthname[d.getMonth()] + ", " + d.getFullYear())
    }

    function renderTime() {
        var currentTime = new Date();
        var diem = "AM";
        var h = currentTime.getHours();
        var m = currentTime.getMinutes();
        var s = currentTime.getSeconds();
        setTimeout(renderTime, 1000);
        if (h == 0) {
            h = 12;
        } else if (h > 12) {
            h = h - 12;
            diem = "PM";
        }
        if (h < 10) {
            h = "0" + h;
        }
        if (m < 10) {
            m = "0" + m;
        }
        if (s < 10) {
            s = "0" + s;
        }
        var myClock = document.getElementById('clockDisplay');

        $('#clockDisplay').text(h + ":" + m + ":" + s + " " + diem);


    }
})(jQuery);
//Vanvbar Property change on scroll
window.onscroll = function () { myFunction() };

var navbar = document.getElementById("nav");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
      navbar.classList.add("navigation-section-title")
  } else {
      navbar.classList.remove("navigation-section-title");
  }
}

AOS.init({
    duration: 1200,
})

const gallery = document.querySelector('.dirGallery');
const lightbox = document.querySelector('.dirLightbox');
const close = document.querySelector('.closeGallery');
const lightboxImg = lightbox.querySelector('img');
gallery.addEventListener('click', function (event) {
    event.preventDefault();
    lightbox.style.display = 'flex';
    lightboxImg.src = event.target.src || event.target.parentNode.href;
});
close.addEventListener('click', function () {
    lightbox.style.display = 'none';
});

const glightbox = GLightbox({
    selector: '.glightbox'
});


