﻿
const progressCircle = document.querySelector(".autoplay-progress svg");
const progressContent = document.querySelector(".autoplay-progress span");
var swiper = new Swiper(".swipercarosal", {
    spaceBetween: 30,
    centeredSlides: true,
    autoHeight: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
    },
    on: {
        autoplayTimeLeft(s, time, progress) {
            progressCircle.style.setProperty("--progress", 1 - progress);
            progressContent.textContent = `${Math.ceil(time / 1000)}s`;
        }
    }
});
var swiper = new Swiper(".newsSwiper", {
    direction: "vertical",
    mousewheel: true,
    spaceBetween: 10,
    //pagination: {
    //    el: '.swiper-pagination',
    //    clickable: false,
    //    type: 'bullets',
    //    renderBullet: function (index, className) {
    //        return '<span class="' + className + '">' + ('0' + (index + 1)) + '</span>';
    //    }
    //},
    //pagination: {
    //    el: ".swiper-pagination",
    //    type: "fraction",
    //    clickable: true,
    //},
    autoplay: {
        delay: 4000,
        disableOnInteraction: false
    },
});


var swiper = new Swiper(".swipSchemes", {
    speed: 600,
    parallax: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

    var swiper = new Swiper(".swipMainSchemes", {
      direction: "vertical",
      slidesPerView: "auto",
      freeMode: true,
      scrollbar: {
        el: ".swiper-scrollbar",
      },
        mousewheel: true,
    });