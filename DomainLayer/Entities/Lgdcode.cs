﻿namespace BirsaKisanApp.DomainLayer.Entities
{
    public class Lgdcode
    {
        public int DistrictId { get; set; }
        public string? DistrictName { get; set; }
        public int BlockId { get; set; }
        public string? BlockName { get; set; }
        public int VillageId { get; set; }
        public string? VillageName { get; set; }
    }
}
