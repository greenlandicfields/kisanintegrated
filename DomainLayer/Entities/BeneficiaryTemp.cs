﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Entities
{
    public  class BeneficiaryTemp
    {
        [Key]
        public int SchemeAutoId { get; set; }
        public string? DeptName { get; set; }
        public string? SchemeName { get; set; }
        public string? BankName { get; set; }
        public string? IFSCCode { get; set; }
        public string? AccountNo { get; set; }
        public string? FarmerName { get; set; }
        public string? MobileNo { get; set; }
        public string? AltMobile { get; set; }
        public DateTime DOB { get; set; }
        public int Age { get; set; }
        public string? PDSNo { get; set; }
        public string? FatherorHusbandName { get; set; }
        public string? BeneficiaryAadharVolt { get; set; }
        public int Districtcode { get; set; }
        public string? DistrictName { get; set; }
        public string? Circlecode { get; set; }
        public string? CircleName { get; set; }
        public string? Villagecode { get; set; }
        public string? VillageName { get; set; }
        public bool IsEkycDone { get; set; }
        public DateTime? EkycDate { get; set; }
        public string? CSCID { get; set; }
        public decimal AmountOutstanding { get; set; }
        public decimal PaymentAmount { get; set; }
        public DateTime? PaymentDt { get; set; }
        public bool Status { get; set; }
        public int HalkaCode { get; set; }
        public string? HalkaName { get; set; }
        public string? VolumeNo { get; set; }
        public string? PageNo { get; set; }
        public string? KhataNo { get; set; }
        public string? PlotNo { get; set; }
        public string? RayatType { get; set; }
        public string? Area_Acer { get; set; }
        public string? Area_Decimil { get; set; }
        public string? MaujaName { get; set; }
        public string? MaujaCode { get; set; }
    }

    
   

}
