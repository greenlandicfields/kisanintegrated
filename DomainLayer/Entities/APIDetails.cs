﻿using System.ComponentModel.DataAnnotations;

namespace BirsaKisanApp.DomainLayer.Entities
{
    public class APIDetails
    {
        public int Id { get; set; }
        public string? ProjectName { get; set; }
        public string? UrlName { get; set; }
        public string? PersonInCharge { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}
