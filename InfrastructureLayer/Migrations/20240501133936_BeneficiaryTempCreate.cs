﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InfrastructureLayer.Migrations
{
    /// <inheritdoc />
    public partial class BeneficiaryTempCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BeneficiaryTemps",
                columns: table => new
                {
                    SchemeAutoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeptName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SchemeName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BankName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IFSCCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AccountNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FarmerName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MobileNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AltMobile = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DOB = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Age = table.Column<int>(type: "int", nullable: false),
                    PDSNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FatherorHusbandName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BeneficiaryAadharVolt = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Districtcode = table.Column<int>(type: "int", nullable: false),
                    DistrictName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Circlecode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CircleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Villagecode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VillageName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsEkycDone = table.Column<bool>(type: "bit", nullable: false),
                    EkycDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    CSCID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AmountOutstanding = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PaymentAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PaymentDt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Status = table.Column<bool>(type: "bit", nullable: false),
                    HalkaCode = table.Column<int>(type: "int", nullable: false),
                    HalkaName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VolumeNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PageNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KhataNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PlotNo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RayatType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Area_Acer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Area_Decimil = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaujaName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MaujaCode = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BeneficiaryTemps", x => x.SchemeAutoId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BeneficiaryTemps");
        }
    }
}
