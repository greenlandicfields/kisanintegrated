﻿using ApplicationLayer.Contracts;
using ApplicationLayer.DTOs;
using Dapper;
using DomainLayer.Entities;
using InfrastructureLayer.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfrastructureLayer.Impl
{
    public class EmployeeRepo : IEmployee
    {
        private readonly AppDbContext appDbContext;
        private readonly IDbConnection _dbConnection;
        public EmployeeRepo(AppDbContext appDbContext, IDbConnection dbConnection)
        {
            this.appDbContext = appDbContext;
            _dbConnection = dbConnection;

        }
        public async Task<ServiceResponse> AddAsync(Employee employee)
        {
            appDbContext.Employees.Add(employee);
            await SaveChangesAsync();
            return new  ServiceResponse(true, "Added...");

        }

        public async Task AddPeople(List<Employee> people)
        {
            await _dbConnection.ExecuteAsync("INSERT INTO Employees (Name, Address) VALUES (@Name, @Address)", people);
        }

        public async Task<List<Employee>> GetAsync() => await appDbContext.Employees.AsNoTracking().ToListAsync();


        /*
         * public async Task<int> AddPerson(Person person)
           {
            string query = "INSERT INTO Employees (Name, Address) VALUES (@Name, @Address)";
            return await _dbConnection.ExecuteAsync(query, person);
           }
         */
        public async Task<Employee> GetByIdAsync(int id) => await appDbContext.Employees.FindAsync(id);

        private async Task SaveChangesAsync() => await appDbContext.SaveChangesAsync();
        
    }
}
