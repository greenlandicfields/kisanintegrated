﻿using ApplicationLayer.Contracts;
using Dapper;
using DomainLayer.Entities;
using InfrastructureLayer.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfrastructureLayer.Impl
{
    public class BeneficiaryTempRepo : IBeneficiaryTemp
    {
        private readonly AppDbContext appDbContext;
        private readonly IDbConnection _dbConnection;
        public BeneficiaryTempRepo(AppDbContext appDbContext, IDbConnection dbConnection)
        {
            this.appDbContext = appDbContext;
            _dbConnection = dbConnection;

        }
        public async Task AddBeneficiary(List<BeneficiaryTemp> beneficiaries)
        {
            string query = "INSERT INTO BeneficiaryTemps (DeptName, SchemeName, BankName, IFSCCode, AccountNo, FarmerName, MobileNo, AltMobile, DOB, Age, PDSNo, FatherorHusbandName, BeneficiaryAadharVolt, Districtcode, DistrictName, Circlecode, CircleName, Villagecode, VillageName, IsEkycDone, EkycDate, CSCID, AmountOutstanding, PaymentAmount, PaymentDt, Status, HalkaCode, HalkaName, VolumeNo, PageNo, KhataNo, PlotNo, RayatType, Area_Acer, Area_Decimil, MaujaName, MaujaCode) " +
                           "VALUES (@DeptName, @SchemeName, @BankName, @IFSCCode, @AccountNo, @FarmerName, @MobileNo, @AltMobile, @DOB, @Age, @PDSNo, @FatherorHusbandName, @BeneficiaryAadharVolt, @Districtcode, @DistrictName, @Circlecode, @CircleName, @Villagecode, @VillageName, @IsEkycDone, @EkycDate, @CSCID, @AmountOutstanding, @PaymentAmount, @PaymentDt, @Status, @HalkaCode, @HalkaName, @VolumeNo, @PageNo, @KhataNo, @PlotNo, @RayatType, @Area_Acer, @Area_Decimil, @MaujaName, @MaujaCode)";

            await _dbConnection.ExecuteAsync(query, beneficiaries);
            //await _dbConnection.ExecuteAsync("INSERT INTO BeneficiaryTemps (Name, Address) VALUES (@Name, @Address)", beneficiaries);
        }

        public async Task<List<BeneficiaryTemp>> GetAsync() => await appDbContext.BeneficiaryTemps.AsNoTracking().ToListAsync();


        public async Task<BeneficiaryTemp> GetByIdAsync(int id) => await appDbContext.BeneficiaryTemps.FindAsync(id);

    }
}
