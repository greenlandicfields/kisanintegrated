﻿using BirsaKisanApp.DomainLayer.Entities;
using DomainLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InfrastructureLayer.Data
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {

        }
        public DbSet<APIDetails> APIManagement { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<BeneficiaryTemp> BeneficiaryTemps { get; set; }


        
    }
}
