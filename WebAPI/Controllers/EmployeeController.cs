﻿using ApplicationLayer.Contracts;
using ApplicationLayer.DTOs;
using DomainLayer.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployee employee;

        public EmployeeController(IEmployee employee)
        {
            this.employee = employee;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = await employee.GetAsync();
            return Ok(data);
        }


        [HttpPost]
        public async Task<IActionResult> Post(List<Employee> people)
        {
            await employee.AddPeople(people);
            int count = people.Count;
            // return Ok(new ServiceResponse(true,$"Records inserted successfully."));
            return Ok(new { Message = $"{count} record(s) inserted successfully.", Count = count });
        }



        /*[HttpPost]
            public async Task<IActionResult> Post(List<Person> people)
            {
                int count = 0;
                foreach (var person in people)
                {
                    count += await _personRepository.AddPerson(person);
                }

                return Ok(new { Message = $"{count} record(s) inserted successfully.", Count = count });
            }
            */


        //[HttpPost]
        //public async Task<IActionResult> Add([FromBody] Employee empDto)
        //{
        //    var result = await employee.AddAsync(empDto);
        //    return Ok(result);
        //}

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var data = await employee.GetByIdAsync(id);
            return Ok(data);
        }




    }
}
