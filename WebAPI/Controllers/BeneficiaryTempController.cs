﻿using ApplicationLayer.Contracts;
using DomainLayer.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeneficiaryTempController : ControllerBase
    {
        private readonly IBeneficiaryTemp beneficiary;

        public BeneficiaryTempController(IBeneficiaryTemp beneficiary)
        {
            this.beneficiary = beneficiary;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = await beneficiary.GetAsync();
            return Ok(data);
        }


        [HttpPost]
        public async Task<IActionResult> Post(List<BeneficiaryTemp> people)
        {
            await beneficiary.AddBeneficiary(people);
            int count = people.Count;
            // return Ok(new ServiceResponse(true,$"Records inserted successfully."));
            return Ok(new { Message = $"{count} record(s) inserted successfully.", Count = count });
        }



        /*[HttpPost]
            public async Task<IActionResult> Post(List<Person> people)
            {
                int count = 0;
                foreach (var person in people)
                {
                    count += await _personRepository.AddPerson(person);
                }

                return Ok(new { Message = $"{count} record(s) inserted successfully.", Count = count });
            }
            */


        //[HttpPost]
        //public async Task<IActionResult> Add([FromBody] Employee empDto)
        //{
        //    var result = await employee.AddAsync(empDto);
        //    return Ok(result);
        //}

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var data = await beneficiary.GetByIdAsync(id);
            return Ok(data);
        }
    }
}
